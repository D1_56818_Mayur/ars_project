
create table flight(
flight_id INTEGER primary key auto_increment,
airline_name VARCHAR(40) ,
departure_airport_name VARCHAR(40),
arrival_airport_name VARCHAR(40),
source_city VARCHAR(40),
destination_city VARCHAR(40),
flight_total_capacity INTEGER,
business_class_capacity INTEGER,
economy_class_capacity INTEGER,
business_class_fare DOUBLE ,
economy_class_fare DOUBLE
);

insert into flight(airline_name,
departure_airport_name,
arrival_airport_name,
source_city,
destination_city,
flight_total_capacity,
business_class_capacity,
economy_class_capacity,
business_class_fare,
economy_class_fare) values('Air India','PNQ', 'MBM','Pune','Mumbai',250,100,150,1500,1000);



create table schedule(
schedule_id INTEGER primary key auto_increment,
flight_id INTEGER,
departure_date DATE,
departure_time TIME,
arrival_date DATE,
arrival_time TIME,
flight_status VARCHAR(40),
FOREIGN KEY(flight_id) REFERENCES flight(flight_id) ON DELETE CASCADE ON UPDATE CASCADE );

insert into schedule (flight_id,departure_date,arrival_date,flight_status) values(1,'2022-03-26','2022-03-27','scheduled');

ALTER TABLE flight ADD UNIQUE airline_dept_arr (airline, departure_airport_name, arrival_airport_name);


{
    "airlineName":"Quatar Airways",
	"departureAirportName":"KAN",
	"arrivalAirportName":"MAD",
	"sourceCity":"Kanpur",
	"destinationCity":"Chennai",
	"businessClassFare":5500,
	"economyClassFare":3500,
	"businessClassCapacity":150,
	"economyClassCapacity":205,
	"flightTotalCapacity":350,
    "departureDate":"2022-03-28",
	"departureTime":"15:00:00",
	"arrivalDate":"2022-03-28",
	"arrivalTime":"16:30:00",
	"flightStatus":"Schedule"
}

select  f.flight_id, s.departure_time, s.arrival_time, CONCAT(
   MOD(HOUR(TIMEDIFF(s.arrival_time,s.departure_time)), 24), ' HOURS ',
   MINUTE(TIMEDIFF(s.arrival_time,s.departure_time)), ' MINUTES ') AS DESCRIPTION,f.economy_class_fare, f.business_class_fare from flight f inner join schedule s on f.flight_id=s.flight_id;

SELECT CONCAT(
   MOD(HOUR(TIMEDIFF(arrival_time,departure_time)), 24), ' HOURS ',
   MINUTE(TIMEDIFF(arrival_time,departure_time)), ' MINUTES ') AS DESCRIPTION
   FROM schedule;


select timestampdiff(MINUTE,arrival_time,departure_time) as details from schedule;
