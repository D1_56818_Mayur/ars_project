package com.app.airline.services;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.airline.dao.IFlightDao;
import com.app.airline.dao.IScheduleDao;
import com.app.airline.dtos.DtotoEntityConvertor;
import com.app.airline.dtos.FlightDto;
import com.app.airline.dtos.FlightScheduleDto;
import com.app.airline.dtos.FlightScheduleDtoProjection;
import com.app.airline.pojos.Flight;
import com.app.airline.pojos.Schedule;

@Transactional
@Service
public class FlightServiceImpl {
	@Autowired
	private IFlightDao fDao;
	@Autowired
	private IScheduleDao sDao;
	
	@Autowired
	private DtotoEntityConvertor convertor;
	
	@Autowired
	private EntityManager entityManager;
	
	
	public Flight findFlightById(int id) {
		Flight f1=fDao.findByFlightId(id);
		
		return f1;
	}
	
	public List<Flight> findAllFlights() {
		List<Flight> flightList=fDao.findAll();
		return flightList;
	}
	
	public Map<String, Object> addFlightAndSchedule(FlightDto flight) {
		Flight flight1 = convertor.toFlightEntity(flight);
		flight1 = fDao.save(flight1);
		Map<String, Object> map=new HashMap<String, Object>();
//		return Collections.singletonMap("insertedId", flight1.getId());
		map.put("insertedFlightId", flight1.getFlightId());
		Schedule sc = convertor.toScheduleEntity(flight);
		sc.setFlight(flight1);
		sc = sDao.save(sc);
		map.put("insertedScheduleId", sc.getScheduleId());
		return map;
		
	}
	
//	public Map<String, Object> addFlight(FlightDto flight) {
//		Flight f = convertor.toFlightEntity(flight);
//		f = fDao.save(f);
//		return Collections.singletonMap("insertedFlightId", f.getFlightId());
//	}

	//Add Flight
		public Flight addFlight(Flight flight) {
			return fDao.save(flight);
		}
	
//	public List<FlightScheduleDto> getFlightDetails(){
//		List<FlightScheduleDto> list=sDao.getFlightScheduleDetails();
//		return list;
//	}
//
//	public List<FlightScheduleDto> searchFlights(FlightDto dto) {
//		String destinationCity = dto.getDestinationCity();
//		String sourceCity = dto.getSourceCity();
//		Date departureDate = dto.getDepartureDate();
//		List<FlightScheduleDto> list=sDao.getFlights(destinationCity, sourceCity, departureDate);
//		return list;
//	}

	public List<FlightScheduleDto> searchFlights1(String destination, String source, Date departureDate) {
		List<FlightScheduleDto> list=sDao.getFlights1(destination, source, departureDate);
				
		return list;
	}
	
	//Find all
		public List<Flight> findAllFlight() {
			return fDao.findAll();
		}
		// Update Flight
		public Flight updateFlight(Flight flight) {

			return fDao.save(flight);
		}

	//Delete Flight By id
		public int deletePassenger(int id) {
			if (fDao.existsById(id)) {
				fDao.deleteById(id);
				return 1;
			} else
				return 0;
		}
		
		//Check flight status
				public List<FlightScheduleDtoProjection> checkFlightStatus(int flightid, Date departureDate){
					List<FlightScheduleDtoProjection> list= fDao.checkFlightStatus(flightid, departureDate);
					if(list.isEmpty()) 
					return null;
						return list;
			}
}
