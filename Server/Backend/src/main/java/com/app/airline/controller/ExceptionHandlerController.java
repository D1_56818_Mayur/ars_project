package com.app.airline.controller;

import java.util.List;
import java.util.stream.Stream;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.app.airline.dtos.Response;

//global exception handling -- applicable for exception arised in any controller
@RestControllerAdvice
public class ExceptionHandlerController {
	
@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<?> validationExceptionHandler(MethodArgumentNotValidException ex){
	
	//Local class for field error
	class fieldErrorDto{
		private String fieldName;
		private String errorMessage;
		
		public fieldErrorDto(String fieldName, String errorMessage) {
			this.fieldName = fieldName;
			this.errorMessage = errorMessage;
		}

		public String getFieldName() {
			return fieldName;
		}

		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}

		public String getErrorMessage() {
			return errorMessage;
		}

		public void setErrorMessage(String errorMessage) {
			this.errorMessage = errorMessage;
		}
				
	}
	
	List<FieldError> errors=ex.getFieldErrors();
		Stream<Object> result=errors.stream().map(err -> new fieldErrorDto(err.getField(), err.getDefaultMessage()));
		return Response.error(result);
	}

@ExceptionHandler(DataIntegrityViolationException.class)
public ResponseEntity<?> dbConstraintHandler(){
	return Response.error("DB constaints Violated..!!");
}
}
