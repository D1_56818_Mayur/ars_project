package com.app.airline.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.airline.pojos.Passenger;

public interface IPassenger extends JpaRepository<Passenger, Integer>{
	
	Passenger findByFirstNameAndLastNameAndEmail(String firstName, String lastName, String email);
	Passenger findById(int id);

}
