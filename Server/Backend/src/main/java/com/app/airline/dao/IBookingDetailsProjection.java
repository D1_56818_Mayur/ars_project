package com.app.airline.dao;

import java.util.Date;

public interface IBookingDetailsProjection {
Integer getBooking_id();
String getTicket_status();
Integer getSeat_no();
Double getTotal_fare();
Date getBooking_date();
String getAirline_name();
Integer getFlight_id();
String getSource_city();
String getDestination_city();
String getDeparture_airport_name();
String getArrival_airport_name();
Date getArrival_time();
Date getDeparture_time();
Date getArrival_date();
Date getDeparture_date();
String getFirst_name();
String getLast_name();

}
