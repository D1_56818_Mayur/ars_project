package com.app.airline.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.airline.dao.OffersDao;
import com.app.airline.pojos.Offers;

@Transactional
@Service
public class OffersServiceImpl {
	@Autowired
	private OffersDao offerDao;

	//Find by id
	public Offers findOfferById(int offerId) {
		return offerDao.findById(offerId);
	}

	// find all offers
	public List<Offers> findAllOfferss() {
		return offerDao.findAll();
	}

	// Add offer
	public int addOffer(Offers Offers) {
		return offerDao.addOffer(Offers.getPromoCode(), Offers.getDiscount(),Offers.getMinTxnAmount() );
	}
	
	// Update offer
	public Offers updateOffer(Offers Offers) {

		return offerDao.save(Offers);
	}

	//Delete Offer
	public int deleteOffersById(int id) {

		if (offerDao.existsById(id)) {
			offerDao.deleteById(id);
			return 1;
		} else
			return 0;
	}

}
