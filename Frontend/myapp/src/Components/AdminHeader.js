import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useNavigate } from 'react-router';
import { toast } from 'react-toastify';
import DropdownItem from 'react-bootstrap/esm/DropdownItem';
import DropdownMenu from 'react-bootstrap/esm/DropdownMenu';

import { Link } from 'react-router-dom';
const styles = {
    title: {
        fontFamily:'Courier New',
        fontSize: '1.1em',
        fontWeight: 'bold',
        color: 'white',
        marginLeft:'25px',
        marginRight:'30px',
        padding:'0',
        marginTop:'7.5px'
    },
    title1: {
      fontFamily:'Courier New',
      fontSize: '1.1em',
      fontWeight: 'bold',
      color: 'white',
      position:"absolute",
      marginLeft:'260px',
      marginRight:'30px',
      top:'25px',
      padding:'0',
  },
    login: {
        fontFamily:'Courier New',
        marginLeft:'0',
        marginRight:'-5',
        color:'white', 
        fontWeight:'bold',
        fontSize:'1.1em'
    },
    logout: {
        fontFamily:'Courier New',
        marginLeft:'0',
        marginRight:'-5',
        color:'white', 
        fontWeight:'bold',
        fontSize:'1.1em',
        backgroundColor:'#5C0632'
    },
    signup: {
        fontFamily:'Courier New',
        margin:'0',
        marginRight:'55px',
        color:'white', 
        fontWeight:'bold',
        fontSize:'1.1em',
    },
    logo: {
      marginLeft: '5px',
      width:'200px',
      height:'50px',
    },
    userlogo: {
        marginLeft: '0px',
        width:'30px',
        height:'30px',
        marginTop:'7px'
      },
    navbar: {
    backgroundColor:'#5C0632',
    position:'top',
    },
  }



const AdminHeader=()=> {
    const navigate =useNavigate()
    const Logout = () => {
      
         sessionStorage.removeItem('firstName')
         sessionStorage.removeItem('lastName')
         sessionStorage.removeItem('id')
         sessionStorage.removeItem('role')
         sessionStorage['loginStatus'] = 0
    toast.success('Logged Out Successfully')
       navigate('/')
    }

  const { firstName, lastName }=sessionStorage
    return (

    <header>
    <nav class="navbar navbar-expand-md navbar-light " style={styles.navbar}>
        <div class="container-fluid topcolor">
            <a href="/" class="navbar-brand">
                <img src={require("./images/000679-Aircraft-02.png")} style={styles.logo} alt="CoolBrand"/>
            </a>
            <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse" >
                <div class="navbar-nav" >
                <div className="btn-group " role="group">
                    <button
                  id="flight"
                  type="button"
                  className="btn btn-primary border-0 dropdown-toggle"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                  style={{backgroundColor:'#5C0632', marginLeft:"75px",fontFamily:'Courier New', fontWeight:'bold', fontSize:"1.1em"}}
                  >Flight
                </button >
                <ul className="dropdown-menu" aria-labelledby="btnGroupDrop1" style={{ marginLeft:"90px"}}>
                  <li>
                    <Link  to="/editflight" className="dropdown-item">
                      View flights 
                    </Link>
                  </li>
                  <li>
                    <Link  to="/addflight" className="dropdown-item">
                      Add flight
                    </Link>
                  </li>
                </ul>
                </div >
                <div class="navbar-nav" style={{ marginLeft: "100px" }}>
                <a href="/bookings" class="nav-item nav-link active" style={styles.title}>Bookings</a>
              </div>
                <div className="btn-group " role="group">
                <button
                  id="offer"
                  type="button"
                  className="btn btn-primary border-0 dropdown-toggle"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                  style={{backgroundColor:'#5C0632', marginLeft:"100px",fontFamily:'Courier New', fontWeight:'bold', fontSize:"1.1em"}}
                  >Special Offers
                </button>
                <ul className="dropdown-menu" aria-labelledby="btnGroupDrop1" style={{marginLeft:"100px"}}>
                  <li>
                    <Link  to="/editoffer" className="dropdown-item">
                      View/Delete offers 
                    </Link>
                  </li>
                  <li>
                    <Link  to="/addoffer" className="dropdown-item">
                      Add offer
                    </Link>
                  </li>
                </ul>
                </div>

                   
                    <div className="btn-group " role="group">
                <button
                  id="package"
                  type="button"
                  className="btn btn-primary border-0 dropdown-toggle"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                  style={{backgroundColor:'#5C0632', marginLeft:"80px", fontFamily:'Courier New', fontWeight:'bold', fontSize:"1.1em" }}
                  >Packages
                </button>
                <ul className="dropdown-menu" aria-labelledby="btnGroupDrop1"   style={{marginLeft:"100px" }}>
                  <li>
                    <Link  to="/editpackage" className="dropdown-item">
                      View/Delete package 
                    </Link>
                  </li>
                  <li>
                    <Link  to="/addpackage" className="dropdown-item">
                      Add package
                    </Link>
                  </li>
                </ul>
                </div>
                                                         
                </div>
                <div class="navbar-nav ms-auto">
                <a href="/">
                <img src={require("./images/profilepic1.png")} style={styles.userlogo} alt="CoolBrand"/>
            </a>           
            <div className="float-end">
              <div className="btn-group " role="group">
                <button
                  id="btnGroupDrop1"
                  type="button"
                  className="btn btn-primary border-0 dropdown-toggle"
                  data-bs-toggle="dropdown"
                  aria-expanded="false"
                  style={{backgroundColor:'#5C0632',fontFamily:'Courier New', fontWeight:'bold', fontSize:"1.1em"}}
                  >Hello admin, {firstName}
                </button>
                <ul className="dropdown-menu" aria-labelledby="btnGroupDrop1">
                  <li>
                    <Link  to="/profileUpdate" className="dropdown-item">
                      Profile
                    </Link>
                  </li>
                  <li>
                    <Link  to="/resetpass" className="dropdown-item">
                      Change Password
                    </Link>
                  </li>
                  <li>
                    <Link  to="/mybooking" className="dropdown-item">
                      My Bookings
                    </Link>
                  </li>
                  <li>
                    <button onClick={Logout} className="dropdown-item">
                      Logout
                    </button>
                  </li>
                </ul>
              </div>
            </div>
            </div>
            </div>
        </div>
    </nav>
    
</header>
    );
  }
 export default AdminHeader