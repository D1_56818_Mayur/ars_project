import axios from "axios"
import { useEffect } from "react"
import { useLocation, useNavigate } from "react-router"
import { toast } from "react-toastify"
import { URL } from "../config"

const CancelBooking = () =>{

    const { state } = useLocation()
    const { bookingid, bookingStatus } = state
    const navigate = useNavigate()
    console.log(bookingid)

    useEffect( () =>{
        CancelSelectedBooking()
                    } ,[] )

function CancelSelectedBooking(){
if(bookingStatus=="Cancelled"){
toast.error('Tickit is already cancelled...!!!!')
navigate('/mybooking') 
}
else{
    const url=`${URL}/user/booking/cancelTicket/${bookingid}`
    axios.put(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success'){
                toast.success('Booking cancelled successfully') 
                navigate('/mybooking')       
        }else{
            toast.error(result['error'])
            navigate('/mybooking')       
        }

    })
}
}

return (
    <div></div>
        )
}
export default CancelBooking