import { useLocation, useNavigate } from "react-router";
import CurrencyInput from 'react-currency-input-field'
import { useState } from "react";
import { toast } from "react-toastify";
import axios from "axios";
import { URL } from "../../config";
import HeaderSelector from "../../Components/HeaderSelector";
import Footer from "../../Components/Footer";
import Content4 from "../../Components/Content4";

const styles = {
  td: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "485px",
  },
  td1: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
  },
  span: {
    fontWeight: "bold",
  },
  td2: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "40.80%",
  },
  td3: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "40%",
  },
  td4: {
    paddingLeft: "20px",
    paddingRight: "20px",
    paddingBottom: "20px",
    width: "39.65%",
  },
  td5: {
    paddingLeft: "20px",
    fontWeight: "bold",
    color: "black",
    width: "39.65%",
  },
  div: {
    marginLeft: "140px",
  },
};
const AddSchedule = () => {
  const navigate = useNavigate()
  const { state } = useLocation()
  const { flight } = state
  const [flightId, setFlightId] = useState(flight.flightId)
  const [departureDate, setDepartureDate] = useState('')
  const [arrivalDate, setArrivalDate] = useState('')
  const [departureTime, setDepartureTime] = useState('')
  const [arrivalTime, setArrivalTime] = useState('')
  const [flightStatus, setFlightStatus] = useState('')


  const AddNewSchedule = () => {
  if (departureDate.length == 0) {
      toast.warning('Please enter departure date')
    } else if (departureTime.length == 0) {
      toast.warning('Please enter departure time')
    } else if (arrivalDate.length == 0) {
      toast.warning('Please enter arrival date')
    } else if (arrivalTime.length == 0) {
      toast.warning('Please enter arrival time')
    } else {
      console.log(flightId)
      const body = {
        flightId,
        departureDate,
        arrivalDate,
        departureTime,
        arrivalTime,
        flightStatus,
      }

      // url to call the api
      const url = `${URL}/schedules/add`

      // http method: post
      // body: contains the data to be sent to the API
      axios.post(url, body).then((response) => {
        // get the data from the response
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
          toast.success('Successfully added new schedule')
          // navigate to the signin page
          navigate('/editflight')
        } else {
          toast.error(result['error'])
        }
      })

    }

  }

  return (
    <div style={{ backgroundColor: '#E5E4E2', height: '100%' }}>
      <HeaderSelector />
      <div className="col mt-4 d-flex justify-content-center"><h1>Add Flight Details</h1></div>
      <div
        className="tab-pane fade active show"
        id="faq_tab_1"
        role="tabpanel"
        aria-labelledby="faq_tab_1-tab"
      >
        <div className="container p-3">
          <div style={styles.div}>
            <table>
              <tbody>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Airline name </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Departure airport name </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.airlineName}
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.departureAirportName}
                      type="text"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Arrival airport name</label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Source city </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.arrivalAirportName}
                      type="text"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.sourceCity}
                      type="text"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Destination city </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Business class fare </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.destinationCity}
                      type="text"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.businessClassFare}
                      type="number"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Economy class fare </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Business class capacity </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.economyClassFare}
                      type="number"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.businessClassCapacity}
                      type="number"
                      readOnly
                      className="form-control"
                    />

                  </td>
                </tr>
                <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                    <label htmlFor="airlinename">Economy class capacity </label>
                  </td>
                  <td style={styles.td1}>
                    <label htmlFor="depname">Flight total capacity </label>
                  </td>
                </tr>
                <tr>
                  <td style={styles.td}>
                    <input value={flight.economyClassCapacity}
                      type="number"
                      readOnly
                      className="form-control"
                    />
                  </td>
                  <td style={styles.td}>
                    <input value={flight.flightTotalCapacity}
                      readOnly
                      type="number"
                      className="form-control"
                    />

                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="row">

          </div>
        </div>
        <div
          className="tab-pane fade active show"
          id="faq_tab_1"
          role="tabpanel"
          aria-labelledby="faq_tab_1-tab"
        >
          <div className="container p-3">
            <div style={styles.div}>
              <table>
                <tbody>
                  <tr style={{ color: "white" }}>
                    <td style={styles.td1}>
                      <label htmlFor="flightid">Selected flightID </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="departdate">Departure date </label>
                    </td>
                  </tr>
                  <tr>
                    <td style={styles.td}>
                    <input value={flight.flightId} 
             
                      type="number"
                      readOnly
                      className="form-control"
                    />
                    </td>
                    <td style={styles.td}>
                      <input onChange={(e) => {
                        setDepartureDate(e.target.value)
                      }}
                      id="departdate"
                        type="date"
                        className="form-control"
                      />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                      <label htmlFor="deptime">Departure time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="arrivaldate">Arrival date</label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input onChange={(e) => {
                        setDepartureTime(e.target.value)
                      }}
                        type="text"
                        id="deptime"
                        placeholder="hh:mm:ss"
                        className="form-control"
                      />
                      <span>Please enter in "HH:MM:SS" format</span>
                    </td>
                    <td style={styles.td}>
                      <input onChange={(e) => {
                        setArrivalDate(e.target.value)
                      }}
                        type="date"
                        id="arrivaldate"
                        className="form-control"
                      />
                    </td>
                  </tr>
                  <tr style={{ color: "white" }}>
                  <td style={styles.td1}>
                      <label htmlFor="arrivaltime">Arrival time </label>
                    </td>
                    <td style={styles.td1}>
                      <label htmlFor="fstatus">Flight status </label>
                    </td>
                  </tr>
                  <tr>
                  <td style={styles.td}>
                      <input onChange={(e) => {
                        setArrivalTime(e.target.value)
                      }}
                        type="text"
                        id="arrivaltime"
                        placeholder="hh:mm:ss"
                        className="form-control"
                      />
                      <span>Please enter in "HH:MM:SS" format</span>
                    </td>
                    <td style={styles.td}>
                      <input onChange={(e) => {
                        setFlightStatus(e.target.value)
                      }}
                        type="text"
                        value="scheduled"
                        id="fstatus"
                        className="form-control"
                        readOnly
                      />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="row">

              <div className="col mt-4 d-flex justify-content-end">
                <button className="btn btn-primary custom-button px-5 justify-content-end"
                  style={{ backgroundColor: '#5C0632' }}
                  onClick={AddNewSchedule}
                >
                  Add Schedule
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Content4/>
      <Footer/>
    </div>
      );
};

      export default AddSchedule;