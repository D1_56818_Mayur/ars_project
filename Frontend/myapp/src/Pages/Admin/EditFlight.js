import { useEffect, useState } from "react"
import { URL } from "../../config"
import axios from "axios"
import { toast } from "react-toastify"
import { useNavigate } from "react-router"
import HeaderSelector from "../../Components/HeaderSelector"
import Footer from "../../Components/Footer"
import Content4 from "../../Components/Content4"


const EditFlight = () => {
    const [ flights, setFlights] = useState([])
    const navigate = useNavigate()
    
    useEffect(ViewFlights,[])

function ViewFlights(){

    const url=`${URL}/admin/flight/`
    axios.get(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success' && result['data'] !=0){
            setFlights(result['data'])
                toast.info('Showing available flights')       
        }else{
            toast.warning('No flights available now')
            navigate('/adminhome') 
        }

    })
}

    return (
        <div style={{backgroundColor:'#E5E4E2'}}>
            <HeaderSelector/>
            <div>
          <br/>
            <button className="btn btn-warning btn-lg float-end" onClick={()=>{navigate('/addflight')}}>Click here to add new flight</button>
           
            <table className="table table-bordered table-hover table-warning">
                <thead>
                    <tr>
                        <th>Flight ID</th>
                        <th>Airline name</th>
                        <th>Departure airport name</th>
                        <th>Arrival airport name</th>
                        <th>Source city</th>
                        <th>Destination city</th>
                        <th>Business class fare</th>
                        <th>Economy class fare</th>
                        <th>Business class capacity</th>
                        <th>Economy class capacity</th>
                        <th>Flight total capacity</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        flights.map( (f) => (
                            <tr key={f.flightId}>
                                <td>{f.flightId}</td> 
                               <td>{f.airlineName }</td> 
                               <td>{f.departureAirportName }</td>
                                <td>{f.arrivalAirportName }</td>
                                <td>{f.sourceCity }</td>
                                <td>{f.destinationCity }</td>
                                <td>{f.businessClassFare}</td>
                                <td>{f.economyClassFare }</td>
                                <td>{f.businessClassCapacity}</td>
                                <td>{f.economyClassCapacity}</td>
                                <td>{f.flightTotalCapacity}</td>
                                <td>
                                    <button className="btn btn-outline-danger btn-sm"
                                   onClick={()=>{navigate('/deleteflight',{state:{flightId :f.flightId}})}} >Delete Flight</button>
                                   <button className="btn btn-outline-success btn-sm"
                                   onClick={()=>{navigate('/addschedule',{state:{flight :f}})}} >Add Schedule</button>
                                    <button className="btn btn-outline-warning btn-sm"
                                   onClick={()=>{navigate('/viewschedule',{state:{flight :f}})}} >View Schedule</button>
                                </td>
                            </tr>
                        )    
                        )
                    }
                </tbody>
            </table>
            </div>
            <Content4/>
            <Footer/>
        </div>
    )
}

export default EditFlight