import axios from "axios"
import { useEffect } from "react"
import { useLocation, useNavigate } from "react-router"
import { toast } from "react-toastify"
import { URL } from "../../config"

const DeleteFlight = () =>{

    const { state } = useLocation()
    const { flightId } = state
    const navigate = useNavigate()
    console.log(flightId)

    useEffect( () =>{
                    DeleteSelectedFlight()
                    } ,[] )

function DeleteSelectedFlight(){

    const url=`${URL}/admin/flight/${flightId}`
    axios.delete(url).then( (response) => {
        const result=response.data
        console.log(result)
        if(result['status']=='success'){
                toast.success('Flight deleted successfully') 
                navigate('/editflight')       
        }else{
            toast.error(result['error'])
            
        }

    })
}

return (
    <div></div>
        )
}
export default DeleteFlight